function getRecentPhotos(){
    $.getJSON( "http://api.flickr.loc/api/v1/photos", function( data ) {
        var photos = [];

        $.each( data['photos']['photo'], function( key, val ) {
            //if exist image then push them to array
            if(val['url_c']) {
                photos.push( "<div id='" + val['id'] + "' class='photo'><a href='../photo.html?id=" + val['id'] + "'><img src='" + val['url_c'] + "' /></a></div>" );
            }
        });

        $('#recentPhotos').append(photos);
    });
}

function getSingePhoto(photo_id) {
    var photoId = !photo_id ? getUrlParameter('id') : null;
    if(!photoId) return; //if we didnt pass photo_id to function or didnt exist such URL parameter

    $.getJSON("http://api.flickr.loc/api/v1/photos/" + photoId + "/sizes", function (data){
        var result = $.grep(data['sizes']['size'], function(e){ return e.label == 'Large'; });

        //get image which we will display on page
        var imageUrl;
        if(result != 0) {
            imageUrl = result[0].source;
        }

        //get sizes
        var sizes = [];

        $.each(data['sizes']['size'], function (key, val) {
            sizes.push("<span class='size'><a href='" + val['source'] + "' >" + val['width'] + "*" + val['height'] +"</a></span>");
        });

        $('.photo').prepend("<img src='" + imageUrl + "'/>");
        $('.sizes').append(sizes);

    });
}

//helper
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};